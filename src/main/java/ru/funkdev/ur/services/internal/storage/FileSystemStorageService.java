package ru.funkdev.ur.services.internal.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileSystemStorageService implements StorageService {

    private StorageProperties storageProperties;

    @Autowired
    public FileSystemStorageService(StorageProperties storageProperties) {
        this.storageProperties = storageProperties;
    }

    @Override
    public void store(MultipartFile file, String dir) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        Path location = Paths.get(this.storageProperties.getRootDir() + dir);
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            try (InputStream inputStream = file.getInputStream()) {
                if (Files.notExists(location)) {
                    Files.createDirectories(location);
                }
                Files.copy(inputStream, location.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);

            }
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
    }

    @Override
    public void delete(String path) throws IOException {
        FileSystemUtils.deleteRecursively(Paths.get(path));
    }

    @Override
    public List<String> getImagesInPath(String path, int depth) throws IOException {
        Path searchPath = Paths.get(path);

        return (ArrayList<String>) Files.walk(searchPath, depth)
                .filter(pathItem -> !pathItem.equals(searchPath))
                .map(pathItem -> pathItem.normalize().toString())
                .collect(Collectors.toList());
    }
}
