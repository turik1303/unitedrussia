package ru.funkdev.ur.db.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "posts")
public class Post implements Serializable {
    private static final long serialVersionUID = 3638492139780308925L;
    @Id
    @SequenceGenerator(name = "posts_id_seq",
            sequenceName = "posts_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "posts_id_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "created_date", nullable = false)
    private Date createdDate;
    @Column(name = "headline", nullable = false)
    private String headline;

    @Column(name = "snippet", columnDefinition = "TEXT", nullable = false)
    private String snippet;
    @Column(name = "headline_image", nullable = true)
    private String headlineImage;
    @Column(name = "post_body", columnDefinition = "TEXT", nullable = false)
    private String postBody;

    @Column(name = "likes_count", nullable = false)
    private Long likesCount;

    @Column(name = "views_count", nullable = false)
    private Long viewsCount;

    @ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    private User author;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL)
    private List<PostImage> postImages;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PostLike> postLikes;

    public Post() {
    }

    public Post(Date createdDate, String headline, String snippet, String headlineImage, String postBody,
                Long likesCount, Long viewsCount, User author, Boolean isActive, List<PostImage> postImages,
                List<PostLike> postLikes) {
        this.createdDate = createdDate;
        this.headline = headline;
        this.snippet = snippet;
        this.headlineImage = headlineImage;
        this.postBody = postBody;
        this.likesCount = likesCount;
        this.viewsCount = viewsCount;
        this.author = author;
        this.isActive = isActive;
        this.postImages = postImages;
        this.postLikes = postLikes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getHeadlineImage() {
        return headlineImage;
    }

    public void setHeadlineImage(String headlineImage) {
        this.headlineImage = headlineImage;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    public Long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Long likesCount) {
        this.likesCount = likesCount;
    }

    public Long getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(Long viewsCount) {
        this.viewsCount = viewsCount;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public List<PostImage> getPostImages() {
        return postImages;
    }

    public void setPostImages(List<PostImage> postImages) {
        this.postImages = postImages;
    }

    public List<PostLike> getPostLikes() {
        return postLikes;
    }

    public void setPostLikes(List<PostLike> postLikes) {
        this.postLikes = postLikes;
    }
}
