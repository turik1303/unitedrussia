package ru.funkdev.ur.controllers.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.funkdev.ur.db.entities.Post;
import ru.funkdev.ur.serviceClasses.Page;
import ru.funkdev.ur.services.data.PostService;

import java.util.List;

@SuppressWarnings("Duplicates")
@Controller
@RequestMapping(value = "/")
public class AllPostsController {
    private static final int PAGE_ITEMS_COUNT = 20;

    private final PostService postService;

    @Autowired
    public AllPostsController(PostService postService) {
        this.postService = postService;
    }

    @RequestMapping(value = "posts", method = RequestMethod.GET)
    public String onPostsPage(@RequestParam(name = "page", required = false) Integer page,
                              @RequestParam(name = "size", required = false) Integer size,
                              Model model) {
        Integer pageNumber = (page != null) ? page : 0;
        Integer pageSize = (size != null) ? size : PAGE_ITEMS_COUNT;
        List<Post> posts = postService.getPostsList(pageNumber, pageSize);

        List<Page> pages = Page.getPages(pageSize, postService.getPostsCount());


        model.addAttribute("posts", posts);
        model.addAttribute("pages", pages);
        return "/posts/index";
    }



}
