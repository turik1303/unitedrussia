package ru.funkdev.ur.db.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "post_likes")
public class PostLike implements Serializable {
    private static final long serialVersionUID = -4684056405835098749L;

    @Id
    @SequenceGenerator(name = "post_like_id_seq", sequenceName = "post_like_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_like_id_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "post_id", nullable = false)
    private Post post;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public PostLike() {
    }

    public PostLike(Post post, User user) {
        this.post = post;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
