package ru.funkdev.ur.db.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "polls")
public class Poll implements Serializable {
    private static final long serialVersionUID = -5101515091892613066L;

    @Id
    @SequenceGenerator(name = "polls_id_seq",
            sequenceName = "polls_id_seq",
            allocationSize = 1)
    @GeneratedValue(generator = "polls_id_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Column(name = "poll_name")
    private String name;

    @Column(name = "is_anonymous")
    private Boolean isAnonymous;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_multiple")
    private Boolean isMultipleSelectionEnabled;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "poll_answers",
            joinColumns = @JoinColumn(name = "poll_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "answer_id", referencedColumnName = "id")
    )
    private List<PollAnswerOption> answerOptions;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "poll_participants",
            joinColumns = @JoinColumn(name = "poll_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private List<User> participants;

    @ManyToOne()
    @JoinColumn(name = "author_id", nullable = false)
    private User author;

    public Poll() {
    }

    public Poll(String name, Boolean isAnonymous, Boolean isActive, Boolean isMultipleSelectionEnabled,
                List<PollAnswerOption> answerOptions, List<User> participants, User author) {
        this.name = name;
        this.isAnonymous = isAnonymous;
        this.isActive = isActive;
        this.isMultipleSelectionEnabled = isMultipleSelectionEnabled;
        this.answerOptions = answerOptions;
        this.participants = participants;
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(Boolean anonymous) {
        isAnonymous = anonymous;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public Boolean getIsMultipleSelectionEnabled() {
        return isMultipleSelectionEnabled;
    }

    public void setIsMultipleSelectionEnabled(Boolean multipleSelectionEnabled) {
        isMultipleSelectionEnabled = multipleSelectionEnabled;
    }

    public List<PollAnswerOption> getAnswerOptions() {
        return answerOptions;
    }

    public void setAnswerOptions(List<PollAnswerOption> answerOptions) {
        this.answerOptions = answerOptions;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
