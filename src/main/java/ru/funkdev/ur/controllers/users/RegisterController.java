package ru.funkdev.ur.controllers.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.funkdev.ur.db.entities.User;
import ru.funkdev.ur.services.data.UserService;

import javax.validation.Valid;

@Controller
public class RegisterController {
    private final UserService userService;

    @Autowired
    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/users/registration", method = RequestMethod.GET)
    public ModelAndView registration () {
        ModelAndView modelAndView = new ModelAndView();
//        User user = new User();
        modelAndView.addObject("user", new UserRegisterModel());
        modelAndView.setViewName("/users/registration");
        return modelAndView;
    }

    @RequestMapping(value = "/users/registration", method = RequestMethod.POST)
    public ModelAndView createUser(@Valid UserRegisterModel userRegisterModel, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        String email = userRegisterModel.getEmail();
        String password = userRegisterModel.getPassword();
        String passwordRepeat = userRegisterModel.getPasswordRepeat();
        boolean userExists = userService.userExists(email);
        if (userExists) {
            bindingResult.rejectValue("error.user", "This user already registered!");
        }
        boolean passwordsNotMatch = !password.equals(passwordRepeat);
        if (passwordsNotMatch) {
            bindingResult.rejectValue("error.user", "Passwords not match!");
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("/users/registration");
        } else {
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "Registration success!");
            modelAndView.addObject("user", new UserRegisterModel());
            modelAndView.setViewName("/users/registration");
        }

        return modelAndView;
    }
    // TODO: refactor this. Move class to models
    private final class UserRegisterModel {

        private String email;
        private String password;
        private String passwordRepeat;

        public UserRegisterModel() {
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPasswordRepeat() {
            return passwordRepeat;
        }

        public void setPasswordRepeat(String passwordRepeat) {
            this.passwordRepeat = passwordRepeat;
        }
    }
}
