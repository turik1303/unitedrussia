package ru.funkdev.ur.controllers.admin.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.funkdev.ur.models.PostModel;
import ru.funkdev.ur.db.entities.Post;
import ru.funkdev.ur.exceptions.NotFoundException;
import ru.funkdev.ur.services.data.PostService;
import ru.funkdev.ur.services.data.UserService;
import ru.funkdev.ur.services.internal.storage.StorageService;

import java.util.Date;

@Controller
@RequestMapping(value = "/admin/posts/")
public class NewPostController {
    private final PostService postService;
    private final UserService userService;
    private final StorageService storageService;

    @Autowired
    public NewPostController(PostService postService, UserService userService, StorageService storageService) {
        this.postService = postService;
        this.userService = userService;
        this.storageService = storageService;
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String onNewPost(Model model) {
        Post draft = new Post(new Date(), "", "", "", "",
                0L, 0L, userService.getCurrentUser(), true, null, null);
        postService.savePost(draft);
        PostModel postModel = new PostModel();
        postModel.setId(draft.getId());
        postModel.setIsActive(draft.getIsActive());
        model.addAttribute("postModel", postModel);
        return "/admin/posts/new";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String onNewPostSave(PostModel postModel) {
        Post newPost = null;
        try {
            newPost = postService.getPostById(postModel.getId());
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        Integer snippetLength = 500;
        assert newPost != null;
        newPost.setId(postModel.getId());
        newPost.setAuthor(userService.getCurrentUser());
        newPost.setCreatedDate(new Date());
        newPost.setHeadline(postModel.getHeadline());
        newPost.setPostBody(postModel.getPostBody());
        newPost.setLikesCount(0L);
        newPost.setViewsCount(0L);
        String snippet = postService.getSnippet(newPost, snippetLength);
        newPost.setSnippet(snippet);
        postService.savePost(newPost);
        String headlineImageUrl = "/images/posts/" + newPost.getId();
        storageService.store(postModel.getHeadlineImageFile(), headlineImageUrl);
        String fileName = postModel.getHeadlineImageFile().getOriginalFilename();
        newPost.setHeadlineImage(headlineImageUrl + "/" + fileName);
        postService.buildPostImages(newPost);
        postService.savePost(newPost);

        return "redirect:/posts/" + newPost.getId().toString() + "/view";
    }


}
