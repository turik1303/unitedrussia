package ru.funkdev.ur.db.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdev.ur.db.entities.Post;

import java.util.Date;
import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findByCreatedDate(Date createdDate);

    List<Post> findByCreatedDateBetween(Date begin, Date end);

    List<Post> findAllByCreatedDateBeforeOrderByCreatedDate(Date createdDate);

    Page<Post> findAll(Pageable pageable);

    void deleteById(Long id);
}
