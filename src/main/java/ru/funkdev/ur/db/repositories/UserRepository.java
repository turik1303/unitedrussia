package ru.funkdev.ur.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdev.ur.db.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
