package ru.funkdev.ur.controllers.admin.polls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.funkdev.ur.db.entities.Poll;
import ru.funkdev.ur.exceptions.NotFoundException;
import ru.funkdev.ur.models.PollModel;
import ru.funkdev.ur.services.data.PollService;

@Controller
@RequestMapping(value = "/admin/polls")
public class EditPollController {
    private final PollService pollService;

    @Autowired
    public EditPollController(PollService pollService) {
        this.pollService = pollService;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public String onPollEdit(@PathVariable String id, Model model) {
        try {
            Poll poll = pollService.getSinglePoll(Long.parseLong(id));
            model.addAttribute("poll", new PollModel(poll));
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return "/admin/polls/edit";
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public String onPollSave(PollModel poll) {

        return "/admin/polls";
    }

}
