package ru.funkdev.ur.models;

import ru.funkdev.ur.db.entities.Poll;

public final class PollModel extends Poll {

    private static final long serialVersionUID = -3856055358547228289L;
    private String action;

    public PollModel() {
        super();
    }

    public PollModel(Poll poll) {
        super();
        this.setAuthor(poll.getAuthor());
        this.setAnswerOptions(poll.getAnswerOptions());
        this.setId(poll.getId());
        this.setIsActive(poll.getIsActive());
        this.setIsAnonymous(poll.getIsAnonymous());
        this.setIsMultipleSelectionEnabled(poll.getIsMultipleSelectionEnabled());
        this.setName(poll.getName());
        this.setParticipants(poll.getParticipants());
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Poll getPoll() {
        return new Poll(this.getName(), this.getIsAnonymous(), this.getIsActive(), this.getIsMultipleSelectionEnabled(),
                this.getAnswerOptions(), this.getParticipants(), this.getAuthor());
    }

    public void updatePollFromModel(Poll poll) {
        if (this.getId() != null) poll.setId(this.getId());
        if (this.getIsActive() != null) poll.setIsActive(this.getIsActive());
        if (this.getIsAnonymous() != null) poll.setIsAnonymous(this.getIsAnonymous());
        if (this.getIsMultipleSelectionEnabled() != null)
            poll.setIsMultipleSelectionEnabled(this.getIsMultipleSelectionEnabled());
        if (this.getAnswerOptions() != null) poll.setAnswerOptions(this.getAnswerOptions());
        if (this.getAuthor() != null) poll.setAuthor(this.getAuthor());
        if (this.getName() != null) poll.setName(this.getName());
        if (this.getParticipants() != null) poll.setParticipants(this.getParticipants());
    }
}
