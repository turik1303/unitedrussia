package ru.funkdev.ur.models;

import org.springframework.web.multipart.MultipartFile;
import ru.funkdev.ur.db.entities.Post;

public final class PostModel extends Post {

    private MultipartFile headlineImageFile;

    public PostModel() {
        super();
    }

    public MultipartFile getHeadlineImageFile() {
        return headlineImageFile;
    }

    public void setHeadlineImageFile(MultipartFile headlineImageFile) {
        this.headlineImageFile = headlineImageFile;
    }
}
