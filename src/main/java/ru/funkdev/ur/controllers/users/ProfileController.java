package ru.funkdev.ur.controllers.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.funkdev.ur.db.entities.User;
import ru.funkdev.ur.db.entities.UserDetails;
import ru.funkdev.ur.services.data.UserService;

@Controller
public class ProfileController {
    private final UserService userService;

    private User user;

    @Autowired
    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/users/profile", method = RequestMethod.GET)
    public ModelAndView onProfilePage() {
        ModelAndView modelAndView = new ModelAndView();

        this.user = userService.getCurrentUser();

        UserProfileModel userProfileModel = new UserProfileModel();
        if (this.user.getUserDetails() != null) {
            userProfileModel.setFirstName(this.user.getUserDetails().getFirstName());
            userProfileModel.setMiddleName(this.user.getUserDetails().getMiddleName());
            userProfileModel.setLastName(this.user.getUserDetails().getLastName());
            userProfileModel.setPostAddress(this.user.getUserDetails().getPostAddress());
        }

        modelAndView.addObject("userProfile", userProfileModel);
        modelAndView.setViewName("/users/profile");
        return modelAndView;
    }


    @RequestMapping(value = "/users/profile", method = RequestMethod.POST)
    public ModelAndView updateProfile(UserProfileModel userProfileModel, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        boolean passwordsEquals = userProfileModel.getPassword().equals(userProfileModel.getPasswordRepeat());
        if (!passwordsEquals) {
            bindingResult.rejectValue("error.profile", "Passwords does not match");
        }
        if ((userProfileModel.getPassword().length() < 8 || userProfileModel.getPasswordRepeat().length() < 8) &&
                (userProfileModel.getPassword().length() > 0 || userProfileModel.getPasswordRepeat().length() > 0)) {
            bindingResult.rejectValue("error.profile", "Password can not be less than 8 symbols");
        }
        if (userProfileModel.getFirstName().length() < 2) {
            bindingResult.rejectValue("error.profile", "Enter valid user firstName");
        }
        if (userProfileModel.getMiddleName().length() < 2) {
            bindingResult.rejectValue("error.profile", "Enter valid user middleName");
        }
        if (userProfileModel.getLastName().length() < 2) {
            bindingResult.rejectValue("error.profile", "Enter valid lastName");
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("/users/profile");
        } else {
            if (this.user.getUserDetails() == null) {
                this.user.setUserDetails(new UserDetails());
            }
            this.user.getUserDetails().setFirstName(userProfileModel.getFirstName());
            this.user.getUserDetails().setMiddleName(userProfileModel.getMiddleName());
            this.user.getUserDetails().setLastName(userProfileModel.getLastName());
            this.user.getUserDetails().setPostAddress(userProfileModel.getPostAddress());
            if (userProfileModel.getPassword().length() > 0)
                this.user.setPassword(userProfileModel.getPassword());
            userService.updateUser(this.user);
        }
        modelAndView.addObject("successMessage", "User profile updated");
        modelAndView.addObject("userProfile", userProfileModel);
        modelAndView.setViewName("/users/profile");
        return modelAndView;
    }
    // TODO: refactor this controller. Move model class to models
    private final class UserProfileModel {
        private String firstName;
        private String middleName;
        private String lastName;
        private String postAddress;
        private String password;
        private String passwordRepeat;

        public UserProfileModel() {
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPostAddress() {
            return postAddress;
        }

        public void setPostAddress(String postAddress) {
            this.postAddress = postAddress;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPasswordRepeat() {
            return passwordRepeat;
        }

        public void setPasswordRepeat(String passwordRepeat) {
            this.passwordRepeat = passwordRepeat;
        }
    }

}
