package ru.funkdev.ur.controllers.admin.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.funkdev.ur.db.entities.Post;
import ru.funkdev.ur.services.data.PostService;

@Controller
@RequestMapping(value = "/admin/posts")
public class DeletePostController {
    private final PostService postService;

    @Autowired
    public DeletePostController(PostService postService) {
        this.postService = postService;
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
    public String onDeletePost(Post post, @PathVariable String id) {
        ModelAndView modelAndView = new ModelAndView();
        postService.deletePost(Long.parseLong(id));
        modelAndView.setViewName("/admin/posts");
        return "redirect:/admin/posts";
    }
}
