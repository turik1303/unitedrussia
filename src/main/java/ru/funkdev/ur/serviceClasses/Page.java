package ru.funkdev.ur.serviceClasses;

import java.util.ArrayList;
import java.util.List;

public class Page {
    private Integer pageNumber;
    private Integer pageSize;

    public Page(Integer pageNumber, Integer pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public static List<Page> getPages(Integer pageSize, Long totalItemsCount) {
        int pageCounter = 0;
        List<Page> pages = new ArrayList<>();
        while (pageCounter * pageSize <= totalItemsCount) {
            int currentPageSize;
            if (totalItemsCount - pageCounter * pageSize > pageSize) {
                currentPageSize = pageSize;
            } else {
                currentPageSize = (int) (totalItemsCount - pageCounter * pageSize);
            }
            pages.add(new Page(pageCounter++, currentPageSize));
        }
        return pages;
    }
}
