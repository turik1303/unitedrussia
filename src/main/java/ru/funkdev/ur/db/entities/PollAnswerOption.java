package ru.funkdev.ur.db.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "poll_answer_options")
public class PollAnswerOption implements Serializable {
    private static final long serialVersionUID = 2828209644974398695L;

    @Id
    @SequenceGenerator(name = "poll_answer_option_id_seq",
            sequenceName = "poll_answer_option_id_seq")
    @GeneratedValue(generator = "poll_answer_option_id_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Column(name = "answer_value")
    private String value;

    @Column(name = "count")
    private Long count;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "poll_answer_users",
            joinColumns = @JoinColumn(name = "answer_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id")
    )
    private List<User> participants;

    public PollAnswerOption() {
    }

    public PollAnswerOption(String value, Long count, List<User> participants) {
        this.value = value;
        this.count = count;
        this.participants = participants;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }
}
