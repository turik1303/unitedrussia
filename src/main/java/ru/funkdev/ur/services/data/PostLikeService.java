package ru.funkdev.ur.services.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.funkdev.ur.db.entities.Post;
import ru.funkdev.ur.db.entities.PostLike;
import ru.funkdev.ur.db.entities.User;
import ru.funkdev.ur.db.repositories.PostLikeRepository;

import java.util.List;

@Service
public class PostLikeService {


    private final PostLikeRepository postLikeRepository;

    private final UserService userService;

    @Autowired
    public PostLikeService(PostLikeRepository postLikeRepository,
                           UserService userService) {
        this.postLikeRepository = postLikeRepository;
        this.userService = userService;
    }

    Post adjustLikes(Post post, User user) {
        List<PostLike> postLikes = postLikeRepository.findByPostAndUser(post, user);
        if (postLikes.isEmpty() || user == null) {
            post.setLikesCount(post.getLikesCount() + 1);
            PostLike newLike = new PostLike(post, user);
            postLikeRepository.saveAndFlush(newLike);
            if (user != null) {
                List<PostLike> userLikes = user.getPostLikes();
                userLikes.add(newLike);
                user.setPostLikes(userLikes);
                userService.updateUserPostLikes(user);
            }
        }
        return post;
    }

}
