package ru.funkdev.ur.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.funkdev.ur.db.entities.User;
import ru.funkdev.ur.services.data.UserService;

@Controller
public class HomeController {
    private final UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home(@RequestParam(name = "error", required = false) Error error) {

        ModelAndView modelAndView = new ModelAndView();


        User currentUser = userService.getCurrentUser();

        modelAndView.addObject("currentUser", currentUser);

        if (error != null) {
            modelAndView.setViewName("/users/login");
        } else {
            modelAndView.setViewName("/index");
        }

        return modelAndView;

    }
}
