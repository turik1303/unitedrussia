package ru.funkdev.ur.controllers.users;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {


    public LoginController() {

    }

    @RequestMapping(value = "/users/login", method = RequestMethod.GET)
    public ModelAndView onLoginPage() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/users/login");
        return modelAndView;
    }

    @RequestMapping(value = "/users/login-error", method = RequestMethod.GET)
    public ModelAndView onLoginError() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/users/login-error");
        return modelAndView;
    }
}
