package ru.funkdev.ur.controllers.admin.polls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.funkdev.ur.db.entities.Poll;
import ru.funkdev.ur.serviceClasses.Page;
import ru.funkdev.ur.services.data.PollService;

import java.util.List;

@Controller
@RequestMapping(value = "/admin")
public class IndexPollController {

    private static final int PAGE_ITEMS_COUNT = 20;
    private final PollService pollService;

    @Autowired
    public IndexPollController(PollService pollService) {
        this.pollService = pollService;
    }

    @RequestMapping(value = "/polls", method = RequestMethod.GET)
    public String onPollsPage(@RequestParam(name = "page", required = false) Integer page,
                              @RequestParam(name = "size", required = false) Integer size,
                              Model model) {
        Integer pageNumber = (page != null) ? page : 0;
        Integer pageSize = (size != null) ? size : PAGE_ITEMS_COUNT;
        List<Poll> polls = pollService.getPollsList(pageNumber, pageSize);
        List<Page> pages = Page.getPages(PAGE_ITEMS_COUNT, pollService.getPollsCount());

        model.addAttribute("polls", polls);
        model.addAttribute("pages", pages);
        return "/admin/polls/index";
    }

}
