package ru.funkdev.ur.services.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.funkdev.ur.db.entities.Role;
import ru.funkdev.ur.db.entities.User;
import ru.funkdev.ur.db.repositories.RoleRepository;
import ru.funkdev.ur.db.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserService(BCryptPasswordEncoder passwordEncoder, UserRepository userRepository, RoleRepository roleRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public void saveUser(User user) {
        Role userRole = roleRepository.findByName("user");
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(userRole);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(userRoles);
        user.setActive(true);
        userRepository.saveAndFlush(user);
    }

    void updateUserPostLikes(User user) {
        userRepository.saveAndFlush(user);
    }

    public void updateUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.saveAndFlush(user);
    }

    public boolean userExists(String email) {
        boolean result;
        try {
            User user = userRepository.findByEmail(email);
            result = user != null;
        } catch (Exception e) {
            result = true;
        }
        return result;
    }

    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();

        return this.findUserByEmail(userName);
    }

    private User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
