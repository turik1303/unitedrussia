package ru.funkdev.ur.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.funkdev.ur.services.internal.storage.StorageService;

// TODO: refactor and make documentation for this

@RestController
public class UploadsController {
    private final StorageService storageService;

    @Autowired
    public UploadsController(StorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping(value = "/uploads/imageUpload", method = RequestMethod.POST)
    public res uploadImage(Req req) {
        System.out.println("upload started");
        String imageUrl = "/images/posts/" + req.getPostId();
        storageService.store(req.getFile(), imageUrl);

        return new res("/uploads" + imageUrl + "/" + req.getFile().getOriginalFilename());
    }

    private class res {
        public res(String location) {
            this.location = location;
        }

        private String location;

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }
    }

    private class Req {

        private String postId;

        private MultipartFile file;

        public Req() {
        }

        public String getPostId() {
            return postId;
        }

        public void setPostId(String postId) {
            this.postId = postId;
        }

        public MultipartFile getFile() {
            return file;
        }

        public void setFile(MultipartFile file) {
            this.file = file;
        }
    }
}
