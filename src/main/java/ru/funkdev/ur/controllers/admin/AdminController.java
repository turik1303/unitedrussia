package ru.funkdev.ur.controllers.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController {
    @RequestMapping(value = "/admin/", method = RequestMethod.GET)
    public ModelAndView adminMain() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/admin/index");
        return modelAndView;
    }

}
