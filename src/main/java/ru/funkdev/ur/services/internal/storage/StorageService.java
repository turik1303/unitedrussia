package ru.funkdev.ur.services.internal.storage;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface StorageService {

    void store(MultipartFile file, String dir);

    void delete(String path) throws IOException;

    List<String> getImagesInPath(String path, int depth) throws IOException;

}
