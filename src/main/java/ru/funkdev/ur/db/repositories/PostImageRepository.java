package ru.funkdev.ur.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdev.ur.db.entities.PostImage;

public interface PostImageRepository extends JpaRepository<PostImage, Long> {
}
