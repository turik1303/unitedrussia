package ru.funkdev.ur.db.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdev.ur.db.entities.Poll;

import java.util.List;
import java.util.Optional;

public interface PollRepository extends JpaRepository<Poll, Long> {

    Page<Poll> findAll(Pageable pageable);

    List<Poll> findAllByIsActive(Boolean active);

    Optional<Poll> findById(Long id);


}
