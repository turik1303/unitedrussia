package ru.funkdev.ur.db.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "post_images")
public class PostImage implements Serializable {
    private static final long serialVersionUID = -5354703310789841078L;
    @Id
    @SequenceGenerator(name = "post_image_id_seq",
            sequenceName = "post_image_id_seq")
    @GeneratedValue(generator = "post_image_id_seq",
            strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "post_id", nullable = true)
    private Post post;

    @Column(name = "image_path")
    private String imagePath;

    public PostImage() {
    }

    public PostImage(Post post, String imagePath) {
        this.post = post;
        this.imagePath = imagePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
