package ru.funkdev.ur.controllers.admin.polls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.funkdev.ur.db.entities.PollAnswerOption;
import ru.funkdev.ur.models.PollModel;
import ru.funkdev.ur.services.data.PollService;
import ru.funkdev.ur.services.data.UserService;

import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Controller
//@Scope("session")
@RequestMapping(value = "/admin/polls")
public class NewPollController implements Serializable {

    private static final long serialVersionUID = -5392902997810490213L;
    private final UserService userService;
    private final PollService pollService;
    private final HttpSession session;

    @Autowired
    public NewPollController(UserService userService, PollService pollService, HttpSession session) {
        this.userService = userService;
        this.pollService = pollService;
        this.session = session;
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String onNewPoll(Model model) {
        PollModel newPoll = getPoll(session);
        if (newPoll == null) {
            newPoll = new PollModel();
            List<PollAnswerOption> options = new ArrayList<>();
            options.add(new PollAnswerOption());
            newPoll.setAnswerOptions(options);
            setPoll(newPoll, session);
        }
        model.addAttribute("poll", newPoll);
        return "/admin/polls/new";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String onSavePoll(PollModel poll) {
        PollModel newPoll = getPoll(session);
        if (poll.getAction().equals("add_option")) {
            newPoll = poll;
            List<PollAnswerOption> options = poll.getAnswerOptions();
            options.add(new PollAnswerOption());
            newPoll.setAnswerOptions(options);
            setPoll(newPoll, session);
            return "redirect:/admin/polls/new";
        } else {
            poll.setAuthor(userService.getCurrentUser());
            pollService.savePoll(poll.getPoll());
            removePoll(session);
            return "/admin/polls";
        }

    }

    private void setPoll(PollModel pollModel, HttpSession httpSession) {
        httpSession.setAttribute("poll", pollModel);
    }

    private PollModel getPoll(HttpSession httpSession) {
        return (PollModel) httpSession.getAttribute("poll");
    }

    private void removePoll(HttpSession httpSession) {
        httpSession.removeAttribute("poll");
    }

}
