package ru.funkdev.ur.services.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.funkdev.ur.db.entities.Poll;
import ru.funkdev.ur.db.repositories.PollRepository;
import ru.funkdev.ur.exceptions.NotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class PollService {
    private final PollRepository pollRepository;

    @Autowired
    public PollService(PollRepository pollRepository) {
        this.pollRepository = pollRepository;
    }

    public Poll getSinglePoll(Long id) throws NotFoundException {
        Optional<Poll> poll = pollRepository.findById(id);
        if (poll.isPresent()) {
            return poll.get();
        } else {
            throw new NotFoundException("Poll with id=" + id.toString() + " not found!");
        }
    }

    public List<Poll> getAllPolls() {
        return pollRepository.findAll();
    }

    public List<Poll> getAllActivePolls() {
        return pollRepository.findAllByIsActive(true);
    }

    public List<Poll> getPollsList(Integer pageNumber, Integer limit) {
        Page<Poll> pollPage = pollRepository.findAll(PageRequest.of(pageNumber, limit));
        return pollPage.getContent();
    }

    public Poll getRandomPoll() {
        List<Poll> allPolls = pollRepository.findAll();
        long randomId = Math.round(Math.random() * allPolls.size());
        return allPolls.get(Math.toIntExact(randomId));
    }

    public void savePoll(Poll poll) {
        pollRepository.saveAndFlush(poll);
    }

    public Long getPollsCount() {
        return pollRepository.count();
    }
}
