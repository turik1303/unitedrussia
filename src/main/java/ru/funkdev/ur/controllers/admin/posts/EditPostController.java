package ru.funkdev.ur.controllers.admin.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.funkdev.ur.models.PostModel;
import ru.funkdev.ur.db.entities.Post;
import ru.funkdev.ur.exceptions.NotFoundException;
import ru.funkdev.ur.services.data.PostService;
import ru.funkdev.ur.services.internal.storage.StorageService;

import java.io.IOException;

@Controller
@RequestMapping(value = "/admin/posts")
public class EditPostController {
    private final PostService postService;
    private final StorageService storageService;

    @Autowired
    public EditPostController(PostService postService, StorageService storageService) {
        this.postService = postService;
        this.storageService = storageService;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public String onEditPost(@PathVariable String id, Model model) {
        try {
            Post post = postService.getPostById(Long.parseLong(id));
            PostModel postModel = new PostModel();
            postModel.setId(post.getId());
            postModel.setCreatedDate(post.getCreatedDate());
            postModel.setAuthor(post.getAuthor());
            postModel.setViewsCount(post.getViewsCount());
            postModel.setSnippet(post.getSnippet());
            postModel.setPostBody(post.getPostBody());
            postModel.setLikesCount(post.getLikesCount());
            postModel.setHeadlineImage(post.getHeadlineImage());
            postModel.setHeadline(post.getHeadline());
            model.addAttribute("post", postModel);
        } catch (NotFoundException e) {
            model.addAttribute("error", "Post with id = " + id + "not found!");
            return "/errors/404";
        }
        return "/admin/posts/edit";
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public String onEditPost(@PathVariable String id,
                                   Model model,
                                   PostModel post,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Error while post editing");
            return "/errors/404";
        } else {
            try {
                Post oldPost = postService.getPostById(post.getId());

                String headlineImageUrl = "/images/posts/" + post.getId();
                if (!post.getHeadlineImageFile().isEmpty()) {
                    storageService.delete("/uploads" + oldPost.getHeadlineImage());
                    storageService.store(post.getHeadlineImageFile(), headlineImageUrl);
                    String fileName = post.getHeadlineImageFile().getOriginalFilename();
                    oldPost.setHeadlineImage(headlineImageUrl + "/" + fileName);
                }

                postService.buildPostImages(oldPost);

                oldPost.setHeadline(post.getHeadline());
                oldPost.setSnippet(post.getSnippet());
                oldPost.setPostBody(post.getPostBody());
                postService.savePost(oldPost);
                return "redirect:/posts/" + oldPost.getId().toString() + "/view";
            } catch (NotFoundException | IOException e) {
                e.printStackTrace();
                return "/errors/404";
            }
        }
    }
}
