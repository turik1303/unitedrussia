package ru.funkdev.ur.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdev.ur.db.entities.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
