package ru.funkdev.ur.services.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.funkdev.ur.db.entities.PostImage;
import ru.funkdev.ur.db.repositories.PostImageRepository;

import java.util.List;

@Service
public class PostImageService {
    private final PostImageRepository postImageRepository;

    @Autowired
    public PostImageService(PostImageRepository postImageRepository) {
        this.postImageRepository = postImageRepository;
    }

    public void saveImages(List<PostImage> images) {
        postImageRepository.saveAll(images);
        postImageRepository.flush();
    }
}
