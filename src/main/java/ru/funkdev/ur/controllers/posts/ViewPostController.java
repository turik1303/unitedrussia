package ru.funkdev.ur.controllers.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.funkdev.ur.db.entities.Post;
import ru.funkdev.ur.exceptions.NotFoundException;
import ru.funkdev.ur.services.data.PostService;

@Controller
@RequestMapping(value = "/posts/")
public class ViewPostController {
    private final PostService postService;

    @Autowired
    public ViewPostController(PostService postService) {
        this.postService = postService;
    }

    @RequestMapping(value = "/{id}/view", method = RequestMethod.GET)
    public String onViewPost(@PathVariable String id, Model model) {
        try {
            Post post = postService.getPostById(Long.parseLong(id));
            postService.adjustPostViews(post);
            model.addAttribute("post", post);
        } catch (NotFoundException e) {
            model.addAttribute("error", "Post with id = " + id + " not found!");
            return "/errors/404";
        }
        return "/posts/view";
    }

    @RequestMapping(value = "/{id}/like", method = RequestMethod.POST)
    public String onLikePost(@PathVariable String id, Model model) {
        Post post;
        try {
            post = postService.getPostById(Long.parseLong(id));
        } catch (NotFoundException e) {
            model.addAttribute("error", "Post with id = " + id + " not found!");
            return "/errors/404";
        }
        postService.adjustPostLikes(post);
        model.addAttribute("post", post);
        return "redirect:/posts/" + post.getId().toString() + "/view";
    }

}
