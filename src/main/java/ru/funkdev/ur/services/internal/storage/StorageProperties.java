package ru.funkdev.ur.services.internal.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {


    public String getRootDir() {
        String rootDir = "uploads";
        return rootDir;
    }

}
