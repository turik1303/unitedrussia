package ru.funkdev.ur.services.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.funkdev.ur.db.entities.Post;
import ru.funkdev.ur.db.entities.PostImage;
import ru.funkdev.ur.db.repositories.PostRepository;
import ru.funkdev.ur.exceptions.NotFoundException;
import ru.funkdev.ur.serviceClasses.Page;
import ru.funkdev.ur.services.internal.storage.StorageService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostService {
    private final PostRepository postRepository;
    private final StorageService storageService;
    private final PostLikeService postLikeService;
    private final UserService userService;
    @Autowired
    public PostService(PostRepository postRepository, StorageService storageService,
                       PostLikeService postLikeService, UserService userService) {
        this.postRepository = postRepository;
        this.storageService = storageService;
        this.postLikeService = postLikeService;
        this.userService = userService;
    }

    public List<Post> getPostsList(Integer pageNumber, Integer limit) {
        org.springframework.data.domain.Page postsPage = postRepository.findAll(PageRequest.of(pageNumber, limit));
        return postsPage.getContent();
    }

    public Post getPostById(Long id) throws NotFoundException {
        Optional<Post> post = postRepository.findById(id);
        if (post.isPresent()) {
            return post.get();
        } else {
            throw new NotFoundException(post.getClass().getName() + " with id = " + id.toString() + " not found");
        }
    }

    public void savePost(Post post) {
        postRepository.saveAndFlush(post);
    }

    public void deletePost(Long id) {
        postRepository.deleteById(id);
    }

    public Long getPostsCount() {
        return postRepository.count();
    }

    public String getSnippet(Post post, Integer snippetLength) {
        return post.getPostBody().length() > 500 ?
                post.getPostBody().substring(0, post.getPostBody().indexOf(" ", snippetLength)) :
                post.getPostBody();
    }

    public void buildPostImages(Post post) {
        try {
            List<String> postImagePaths = storageService.getImagesInPath("uploads/images/posts/" + post.getId().toString(), 1);
            List<PostImage> postImages = postImagePaths.stream()
                    .map(path -> new PostImage(post, path))
                    .collect(Collectors.toList());
            post.setPostImages(postImages);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void adjustPostLikes(Post post) {
        this.savePost(postLikeService.adjustLikes(post, userService.getCurrentUser()));
    }

    public void adjustPostViews(Post post) {
        post.setViewsCount(post.getViewsCount() + 1);
        this.savePost(post);
    }
}
