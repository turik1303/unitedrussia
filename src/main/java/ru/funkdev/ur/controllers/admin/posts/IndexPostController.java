package ru.funkdev.ur.controllers.admin.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.funkdev.ur.db.entities.Post;
import ru.funkdev.ur.serviceClasses.Page;
import ru.funkdev.ur.services.data.PostService;

import java.util.List;

@SuppressWarnings("Duplicates")
@Controller
@RequestMapping(value = "/admin")
public class IndexPostController {
    private static final int DEFAULT_PAGE_SIZE = 50;
    private final PostService postService;

    @Autowired
    public IndexPostController(PostService postService) {
        this.postService = postService;
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String onPostsPage(@RequestParam(name = "page", required = false) Integer page,
                              @RequestParam(name = "size", required = false) Integer size,
                              Model model) {
        Integer pageNumber = (page != null) ? page : 0;
        Integer pageSize = (size != null) ? size : DEFAULT_PAGE_SIZE;
        List<Post> posts = postService.getPostsList(pageNumber, pageSize);
        List<Page> pages = Page.getPages(pageSize, postService.getPostsCount());
        model.addAttribute("posts", posts);
        model.addAttribute("pages", pages);
        return "/admin/posts";
    }

}
