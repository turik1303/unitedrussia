package ru.funkdev.ur.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdev.ur.db.entities.Post;
import ru.funkdev.ur.db.entities.PostLike;
import ru.funkdev.ur.db.entities.User;

import java.util.List;

public interface PostLikeRepository extends JpaRepository<PostLike, Long> {

    List<PostLike> findAllByPostId(Long postId);

    List<PostLike> findByPostAndUser(Post post, User user);
}
